# Documento del PR: Blackjack de Pau Gallego

[JUGAR JUEGO AQUÍ](https://ghalek.itch.io/pr-blackjack)

## Descripción del juego y reglas

Este proyecto consiste en el juego de cartas del Blackjack, el cúal se juega con una baraja de póker.

Para ganar en el Blackjack, hay que conseguir una puntuación superior al del "dealer" pero sin sobrepasar el número 21. Tanto el dealer como el jugador empieza con dos cartas cada uno. El jugador solo puede ver una de las cartas del dealer mientras que la otra estará oculta.

El jugador puede decidir si robar carta o plantarse. En el Blackjack se puede robar tantas cartas como desee el jugador pero aquí estará limitado a 3 robos. Cuando el jugador termine su turno, jugará el dealer el cual tratará de obtener una puntuación mayor al del jugador pero sin sobrepasar el 21.

Los ases valen 11 o 1 dependiendo de la situación. Las figuras; K, Q y J valen 10 siempre y los números; del 2 al 10, valen el número asignado de cada carta.

Si el jugador logra superar al dealer sin pasar de 21, ganará. En cambio si se pasa de 21 o el dealer le supera sin sobrepasar el 21, el jugador perderá. En caso de empate, no pierde ni gana nadie.

## Creación de la baraja

Para crear la baraja se ha utilizado varias matrices las cuales cada matriz representa una característica de la carta cómo su valor, su palo, su dibujo y si está dentro de la baraja o no.

```
public int[] value = new int[52]; // Valor de la carta
public char[] suit = new char[52]; // Palos de la carta
public bool[] returned = new bool[52]; // Si la carta esta en la baraja o no
public Sprite[] frontCard = new Sprite[52]; // Imagen de cada carta 
```

Las sprites de las cartas se han creado mediante Aseprite.

Cada matriz posee 52 slots para las 52 cartas que hay en una baraja. Se ha planteado de esta manera para que cada carta está asociado a un número. Por ejemplo "As de Picas" es la carta número 0; con un valor de 11, con un palo de "p" (picas), etc.

El palo al final no se usa en ningún momento. En un principio se creó para utilizarse junto con modificadores que permiten variar las reglas de la partida pero que fue descartado como ocurre en el juego de Balatro.

Para asignar los valores correspondientes en cada carta, se ha creado este bucle.

```
public void InitializeDeck()
    {
        // INICIALIZACIÓN DE TODAS LAS CARTAS

        for (int i = 0; i < 13; i++)
        {
            // Inicialización de todas las cartas y asignación de palos
            
            value[i] = i + 1; // Picas
            suit[i] = 'p';

            value[i + 13] = i + 1; // Corazones
            suit[i + 13] = 'c';

            value[i + 26] = i + 1; // Tréboles
            suit[i + 26] = 't';

            value[i + 39] = i + 1; // Diamantes
            suit[i + 39] = 'd';

            // Luego se modifican los Ases y las Figuras para su valor correcto
            
            if (i == 0)
            {
                // Modificación de los Ases
                
                value[i] = 11; //As de Picas
                value[i+13] = 11; // As de Corazones
                value[i+26] = 11; // As de Tréboles
                value[i+39] = 11; // As de Diamantes
            }
            if (i > 9)
            {
                // Modificación de las figuras (Rey, Reina y Jota)
                
                value[i] = 10; //Figuras de Picas
                value[i+13] = 10; // Figuras de Corazones
                value[i+26] = 10; // Figuras de Tréboles
                value[i+39] = 10; // Figuras de Diamantes
            }
        }
    }
```

Junto con otra función/bucle que devuelva todas las cartas a la baraja.

```
public void ReturnCards()
    {
        // DEVUELVE TODAS LAS CARTAS A LA BARAJA
        
        for (int i = 0; i < 52; i++)
        {
            returned[i] = true;
        }
    }
```

## Reparto de cartas

En el primer reparto, se reparten 2 cartas al jugador y al dealer solo que una de las cartas del dealer estará oculta al jugador.

Al repartir una carta, el programa genera un número aleatorio el cual se comporta como una ID y mediante la matriz "returned" comprueba si esa carta está dentro de la baraja o no. En caso de que esté dentro de la baraja, entregará la carta asociada con esa ID y le otorgará un valor y una spirite dependiendo de qué carta sea. En caso de que salga un número de carta que no esté dentro de la baraja, se generará otro número aleatorio y así sucesivamente hasta que se genere una carta que esté dentro de la baraja. Este método se aplica en todos los repartos.

Este primer reparto está asociado al botón de DEAL.

```
public void FirstDealCard()
    {
        // REPARTIR LAS CARTAS INICIALES AL JUGADOR Y AL DEALER
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);

        returned[cardId] = false;
        playerCards[0] = value[cardId];
        playerCardSlot[0].sprite = frontCard[cardId];
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);
        
        returned[cardId] = false;
        playerCards[1] = value[cardId];
        playerCardSlot[1].sprite = frontCard[cardId];
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);

        returned[cardId] = false;
        dealerCards[0] = value[cardId];
        dealerCardSlot[0].sprite = frontCard[cardId];
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);
        
        returned[cardId] = false;
        dealerCards[1] = value[cardId];
        cardHidden = cardId;

        CalculePlayerCards();
        CalculeDealerCards();
        playerPosition = 2;
        dealerPosition = 2;
    }
```

Robo de carta del jugador.

Este robo está asociado al botón de HIT.

```
public void DealCard()
    {
        // REPARTIR CARTA AL JUGADOR
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);

        playerCards[playerPosition] = value[cardId];
        playerCardSlot[playerPosition].sprite = frontCard[cardId];
        
        CalculePlayerCards();
        playerPosition++;
    }
```

Turno del dealer para plantarse o robar cartas. Esta fase se activa cuando el jugador se planta.

Esta fase está asociado al botón STAND.

```
public void DealerTurn()
    {
        // REPARTIR CARTA AL DEALER

        dealerCardSlot[1].sprite = frontCard[cardHidden];
        
        if (!failed)
        {
            while (dealerPoints < playerPoints && dealerPoints < BLACKJACK && dealerPosition < 5)
            {
                do
                {
                    cardId = UnityEngine.Random.Range(0, 52);
                } while (!returned[cardId]);

                dealerCards[dealerPosition] = value[cardId];
                dealerCardSlot[dealerPosition].sprite = frontCard[cardId];

                CalculeDealerCards();
                dealerPosition++;
            }
        }
        
        CompareCards();
    }
```

## Cálculo de valores de las cartas

Es una función que se ejecuta cada vez que se reparte una carta. Sirve para sumar todos los valores de las cartas de cada jugador. También sirve para modificar los valores de los ases en caso de que la suma supere el 21.

Por ejemplo; imaginemos que tengo un 5 y dos ases, al valer 11 los ases, el programa sumar 27 puntos y el jugador superara el límite. Por eso, a más a más, se ha creado un bucle que comprueba todas las cartas de la mano para ver si hay algún as que esté perjudicando la mano y modificar el valor de los ases que hagan falta para que el jugador no supere el 21. En el ejemplo anterior, si el jugador tiene un 5 y dos ases, el programa solo modificará un as para obtener una puntuación de 17 (5 + 1 + 11).

```
public void CalculePlayerCards()
    {
        // CALCULAR EL VALOR DE TODAS LAS CARTAS DE LA MANO
        
        for (int i = 0; i < playerCards.GetLength(0); i++)
        {
            playerPoints += playerCards[i];
        }

        // MODIFICAR EL VALOR DE LOS ASES NECESARIOS

        if (playerPoints > BLACKJACK)
        {
            for (int i = 0; i < 11; i++)
            {
                if (playerCards[i] == 11)
                {
                    playerCards[i] = 1;
                }

                // VOLVER A SUMAR LA PUNTUACIÓN
                
                playerPoints = 0;

                for (int j = 0; j < playerCards.GetLength(0); j++)
                {
                    playerPoints += playerCards[j];
                }

                // ROMPER EL BUCLE EN CASO DE LOGRAR UNA PUNTUACIÓN IGUAL O INFERIOR A 21
                
                if (playerPoints <= BLACKJACK)
                {
                    break;
                }

                // DAR POR FALLIDA LA MANO

                if (playerPoints > BLACKJACK)
                {
                    failed = true;
                }
            }
        }
    }

public void CalculeDealerCards()
    {
        // CALCULAR EL VALOR DE TODAS LAS CARTAS DE LA MANO DEL DEALER
        
        for (int i = 0; i < playerCards.GetLength(0); i++)
        {
            dealerPoints += dealerCards[i];
        }
        
        // MODIFICAR EL VALOR DE LOS ASES NECESARIOS DEL DEALER

        if (dealerPoints > BLACKJACK)
        {
            for (int i = 0; i < 11; i++)
            {
                if (dealerCards[i] == 11)
                {
                    dealerCards[i] = 1;
                }

                // VOLVER A SUMAR LA PUNTUACIÓN DEL DEALER
                
                dealerPoints = 0;

                for (int j = 0; j < dealerCards.GetLength(0); j++)
                {
                    dealerPoints += dealerCards[j];
                }
                
                // ROMPER EL BUCLE EN CASO DE LOGRAR UNA PUNTUACIÓN IGUAL O INFERIOR A 21
                
                if (dealerPoints <= BLACKJACK)
                {
                    break;
                }
            }
        }
    }
```
## Comparar resultados y determinar ganador y perdedor

Cuando el dealer termina su turno y están todas las cartas calculadas, se activa esta función que comprara resultado y decide quién ha ganado y quién pierde.

```
public void CompareCards()
    {
        // COMPARACIÓN DE PUNTUACIONES

        if (playerPoints <= BLACKJACK && playerPoints > dealerPoints || playerPoints <= BLACKJACK && dealerPoints > BLACKJACK)
        {
            // GANA JUGADOR
            Debug.Log("Gana jugador");
            infoText.text = "YOU WIN!";
            wins++;
        }
        
        if (playerPoints <= BLACKJACK && playerPoints == dealerPoints)
        {
            // EMPATE
            Debug.Log("Empate");
            infoText.text = "DRAW";
        }
        
        if (playerPoints > BLACKJACK || playerPoints < dealerPoints && dealerPoints <= BLACKJACK)
        {
            // PIERDE JUGADOR
            Debug.Log("Pierde jugador");
            infoText.text = "YOU LOSE";
            loses++;
        }

        winsText.text = wins.ToString();
        losesText.text = loses.ToString();
    }
```
## Nueva partida

Después de ver quién ha ganado o ha perdido, hay que reiniciar todas las puntuaciones, excepto el contador de victorias y derrotas, y devolver las cartas a baraja para jugar otra partida.

```
public void RestartGame()
    {
        // REINICIAR JUEGO
        
        ReturnCards();

        for (int i = 0; i < 5; i++)
        {
            playerCards[i] = 0;
            playerCardSlot[i].sprite = backCard;
            dealerCards[i] = 0;
            dealerCardSlot[i].sprite = backCard;
        }

        failed = false;

        playerPoints = 0;
        dealerPoints = 0;

        CalculePlayerCards();
        CalculeDealerCards();

        infoText.text = "...";
    }
```
## Consejos al jugar el juego

No te frustres. El dealer siempre juega con ventaja contra el jugador.

Hay que tener en cuenta que la IA del dealer está programada para saber que cartas tiene el jugador y de robar cartas siempre y cuando tenga menos puntos que el mismo, aunque eso suponga arriesgar demasiado y sobrepasar el 21.

Por ejemplo, si el jugador tiene 20 o incluso 21 puntos y el dealer solo tiene 19; el dealer se verá obligado a robar debido a que si se planta perderá de todas formas mientras que si roba tendrá una oportunidad, muy pequeña, de ganar o empatar.

## Recursos utilizados

- [ ] Sprites de las cartas, botones y logo: Creación propia.
- [ ] Fuente de texto: [Pixeloid Font Family](https://www.1001fonts.com/pixeloid-font.html) de [GGBotNet](https://www.1001fonts.com/users/GGBotNet/).