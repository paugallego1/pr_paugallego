using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = System.Random;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Blackjack : MonoBehaviour
{
    private const int BLACKJACK = 21; // VALOR LÍMITE DE BLACKJACK
    
    // VARIABLES
    public int[] value = new int[52]; // Valor de la carta
    public char[] suit = new char[52]; // Palos de la carta
    public bool[] returned = new bool[52]; // Si la carta esta en la baraja o no
    public Sprite[] frontCard = new Sprite[52]; // Imagen de cada carta
    public Sprite backCard; // Imagen de la parte trasera de la carta

    public Button standButton;
    public Button hitdButton;
    public Button dealButton;
    public Button nextButton;

    public Text winsText;
    public Text losesText;
    public Text infoText;

    public SpriteRenderer[] playerCardSlot = new SpriteRenderer[5]; // Posición de cartas del jugador
    public SpriteRenderer[] dealerCardSlot = new SpriteRenderer[5]; // Posición de cartas del dealer

    private bool stand;
    private bool hit;
    private bool deal;
    private bool next;
    private bool failed;

    public int playerPoints; // Puntos del jugador
    public int dealerPoints; // Puntos del dealer
    public int cardId; // ID de la carta
    public int cardHidden; // ID de la carta volteada
    public int playerPosition = 0; // Slot de la carta del jugador
    public int dealerPosition = 0; // Slot de la carta del dealer
    public int wins;
    public int loses;

    public int[] playerCards = new int[11]; // Posición de la carta del jugador
    public int[] dealerCards = new int[11]; // Posición de la carta del dealer

    void Start()
    {
        // INICIO
        
        InitializeDeck();
        RestartGame();

        wins = 0;
        loses = 0;
        
        winsText.text = wins.ToString();
        losesText.text = loses.ToString();
    }

    public void InitializeDeck()
    {
        // INICIALIZACIÓN DE TODAS LAS CARTAS

        for (int i = 0; i < 13; i++)
        {
            // Inicialización de todas las cartas y asignación de palos
            
            value[i] = i + 1; // Picas
            suit[i] = 'p';

            value[i + 13] = i + 1; // Corazones
            suit[i + 13] = 'c';

            value[i + 26] = i + 1; // Tréboles
            suit[i + 26] = 't';

            value[i + 39] = i + 1; // Diamantes
            suit[i + 39] = 'd';

            // Luego se modifican los Ases y las Figuras para su valor correcto
            
            if (i == 0)
            {
                // Modificación de los Ases
                
                value[i] = 11; //As de Picas
                value[i+13] = 11; // As de Corazones
                value[i+26] = 11; // As de Tréboles
                value[i+39] = 11; // As de Diamantes
            }
            if (i > 9)
            {
                // Modificación de las figuras (Rey, Reina y Jota)
                
                value[i] = 10; //Figuras de Picas
                value[i+13] = 10; // Figuras de Corazones
                value[i+26] = 10; // Figuras de Tréboles
                value[i+39] = 10; // Figuras de Diamantes
            }
        }
    }

    public void RestartGame()
    {
        // REINICIAR JUEGO
        
        ReturnCards();

        for (int i = 0; i < 5; i++)
        {
            playerCards[i] = 0;
            playerCardSlot[i].sprite = backCard;
            dealerCards[i] = 0;
            dealerCardSlot[i].sprite = backCard;
        }

        failed = false;
        playerPoints = 0;
        dealerPoints = 0;
        CalculePlayerCards();
        CalculeDealerCards();
        stand = false;
        hit = false;
        deal = true;
        next = false;
        CheckButtons();
        infoText.text = "...";
    }
    
    public void ReturnCards()
    {
        // DEVUELVE TODAS LAS CARTAS A LA BARAJA
        
        for (int i = 0; i < 52; i++)
        {
            returned[i] = true;
        }
    }

    public void FirstDealCard()
    {
        // REPARTIR LAS CARTAS INICIALES AL JUGADOR Y AL DEALER
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);

        returned[cardId] = false;
        playerCards[0] = value[cardId];
        playerCardSlot[0].sprite = frontCard[cardId];
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);
        
        returned[cardId] = false;
        playerCards[1] = value[cardId];
        playerCardSlot[1].sprite = frontCard[cardId];
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);

        returned[cardId] = false;
        dealerCards[0] = value[cardId];
        dealerCardSlot[0].sprite = frontCard[cardId];
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);
        
        returned[cardId] = false;
        dealerCards[1] = value[cardId];
        cardHidden = cardId;

        CalculePlayerCards();
        CalculeDealerCards();
        playerPosition = 2;
        dealerPosition = 2;
        
        stand = true;
        hit = true;
        deal = false;
        CheckButtons();
    }
    
    public void DealCard()
    {
        // REPARTIR CARTA AL JUGADOR
        
        do {cardId = UnityEngine.Random.Range(0, 52);}
        while (!returned[cardId]);

        playerCards[playerPosition] = value[cardId];
        playerCardSlot[playerPosition].sprite = frontCard[cardId];
        
        CalculePlayerCards();
        playerPosition++;
        
        stand = true;
        hit = true;
        deal = false;
        CheckButtons();
    }

    public void DealerTurn()
    {
        // REPARTIR CARTA AL DEALER
        
        dealerCardSlot[1].sprite = frontCard[cardHidden];

        if (!failed)
        {
            while (dealerPoints < playerPoints && dealerPoints < BLACKJACK && dealerPosition < 5)
            {
                do
                {
                    cardId = UnityEngine.Random.Range(0, 52);
                } while (!returned[cardId]);

                dealerCards[dealerPosition] = value[cardId];
                dealerCardSlot[dealerPosition].sprite = frontCard[cardId];

                CalculeDealerCards();
                dealerPosition++;
            }
        }

        CompareCards();
    }

    public void CalculePlayerCards()
    {
        // CALCULAR EL VALOR DE TODAS LAS CARTAS DE LA MANO

        stand = false;
        hit = false;
        deal = false;
        CheckButtons();
        
        for (int i = 0; i < playerCards.GetLength(0); i++)
        {
            playerPoints += playerCards[i];
        }

        // MODIFICAR EL VALOR DE LOS ASES NECESARIOS
        
        if (playerPoints > BLACKJACK)
        {
            for (int i = 0; i < 11; i++)
            {
                if (playerCards[i] == 11)
                {
                    playerCards[i] = 1;
                }

                // VOLVER A SUMAR LA PUNTUACIÓN
                
                playerPoints = 0;

                for (int j = 0; j < playerCards.GetLength(0); j++)
                {
                    playerPoints += playerCards[j];
                }
                
                // ROMPER EL BUCLE EN CASO DE LOGRAR UNA PUNTUACIÓN IGUAL O INFERIOR A 21
                
                if (playerPoints <= BLACKJACK)
                {
                    stand = true;
                    hit = true;
                    deal = false;
                    CheckButtons();
                    break;
                }
            }

            // DAR POR FALLIDA LA MANO
            
            if (playerPoints > BLACKJACK)
            {
                failed = true;
                stand = true;
                hit = false;
                deal = false;
                CheckButtons();
            }
        }
    }

    public void CalculeDealerCards()
    {
        // CALCULAR EL VALOR DE TODAS LAS CARTAS DE LA MANO DEL DEALER
        
        for (int i = 0; i < playerCards.GetLength(0); i++)
        {
            dealerPoints += dealerCards[i];
        }
        
        // MODIFICAR EL VALOR DE LOS ASES NECESARIOS DEL DEALER

        if (dealerPoints > BLACKJACK)
        {
            for (int i = 0; i < 11; i++)
            {
                if (dealerCards[i] == 11)
                {
                    dealerCards[i] = 1;
                }

                // VOLVER A SUMAR LA PUNTUACIÓN DEL DEALER
                
                dealerPoints = 0;

                for (int j = 0; j < dealerCards.GetLength(0); j++)
                {
                    dealerPoints += dealerCards[j];
                }
                
                // ROMPER EL BUCLE EN CASO DE LOGRAR UNA PUNTUACIÓN IGUAL O INFERIOR A 21
                
                if (dealerPoints <= BLACKJACK)
                {
                    break;
                }
            }
        }
    }

    public void CompareCards()
    {
        // COMPARACIÓN DE PUNTUACIONES
        
        if (playerPoints <= BLACKJACK && playerPoints > dealerPoints || playerPoints <= BLACKJACK && dealerPoints > BLACKJACK)
        {
            // GANA JUGADOR
            infoText.text = "YOU WIN!";
            wins++;
        }
        
        if (playerPoints <= BLACKJACK && playerPoints == dealerPoints)
        {
            // EMPATE
            infoText.text = "DRAW";
        }
        
        if (playerPoints > BLACKJACK || playerPoints < dealerPoints && dealerPoints <= BLACKJACK)
        {
            // PIERDE JUGADOR
            infoText.text = "YOU LOSE";
            loses++;
        }

        winsText.text = wins.ToString();
        losesText.text = loses.ToString();

        stand = false;
        hit = false;
        deal = false;
        next = true;
        CheckButtons();
    }

    void CheckButtons()
    {
        if (stand) { standButton.interactable = true; }
        else { standButton.interactable = false; }
        
        if (hit) { hitdButton.interactable = true; }
        else { hitdButton.interactable = false; }
        
        if (deal) { dealButton.interactable = true; }
        else { dealButton.interactable = false; }
        
        if (next) { nextButton.interactable = true; }
        else { nextButton.interactable = false; }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}