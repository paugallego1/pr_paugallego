using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject credits;
    public GameObject rulesExplain;
    public Toggle toggleRules;
    public bool skipRules;
    public bool readingRules;

    public void Start()
    {
        MainMenu();
        readingRules = false;
        toggleRules.onValueChanged.AddListener(ToggleValueChanged);
    }
    
    public void Play()
    {
        if (skipRules || readingRules)
        {
            SceneManager.LoadScene("Game");
        }
        else
        {
            RulesExplain();
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Options()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
        credits.SetActive(false);
        rulesExplain.SetActive(false);
    }

    public void Credits()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(false);
        credits.SetActive(true);
        rulesExplain.SetActive(false);
    }

    public void RulesExplain()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(false);
        credits.SetActive(false);
        rulesExplain.SetActive(true);
        readingRules = true;
    }

    public void MainMenu()
    {
        mainMenu.SetActive(true);
        optionsMenu.SetActive(false);
        credits.SetActive(false);
        rulesExplain.SetActive(false);
        readingRules = false;
    }

    public void ToggleValueChanged(bool isOn)
    {
        skipRules = isOn;
        
        if (skipRules)
        {
            Debug.Log("La opción está habilitada.");
        }
        else
        {
            Debug.Log("La opción está deshabilitada.");
        }
    }
}
